extern crate env_logger;

use colored::*;
use memedb_core::{read_tags, write_tags, Error};
use rayon::prelude::*;
use std::collections::HashSet;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
enum Cli {
    #[structopt(name = "read")]
    Read {
        #[structopt(parse(from_str))]
        paths: Vec<PathBuf>,
    },
    #[structopt(name = "write")]
    Write {
        #[structopt(parse(from_str))]
        path: PathBuf,
        #[structopt(parse(from_str))]
        tags: Vec<String>,
    },
}

fn print_tags(tags: HashSet<String>) -> ColoredString {
    match tags.len() {
        0 => "No tags found".to_string().blue(),
        _ => tags
            .iter()
            .cloned()
            .collect::<Vec<String>>()
            .join(", ")
            .green(),
    }
}

fn main() -> Result<(), Error> {
    env_logger::init();
    match Cli::from_args() {
        Cli::Read { paths } => {
            paths.par_iter().for_each(|path| {
                let file = path.file_name().unwrap().to_str().unwrap();
                match read_tags(&path) {
                    Ok(tags) => println!("[{}] {}: {}", "OK".green(), file, print_tags(tags)),
                    Err(err) => println!("[{}] {}: {}", "ER".red(), file, format!("{}", err).red()),
                }
            });
        }
        Cli::Write { path, tags } => write_tags(&path, &tags.iter().cloned().collect())?,
    };
    Ok(())
}
